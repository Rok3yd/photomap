package com.example.detroidsandwich.photomap.UI

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import android.view.View
import android.widget.Toast
import com.example.detroidsandwich.photomap.R
import com.google.firebase.auth.FirebaseAuth


class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private var mAuth: FirebaseAuth? = null
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null
    private var mAuthProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        registerTextView.setOnClickListener(this)
        passwordLoginButton.setOnClickListener(this)

        mAuth = FirebaseAuth.getInstance()
        createAuthProgressDialog()

        mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser != null) {
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }
        }
    }

    public override fun onStart() {
        super.onStart()
        mAuth!!.addAuthStateListener(mAuthListener!!)
    }

    public override fun onStop() {
        super.onStop()
        if (mAuthListener != null) {
            mAuth!!.removeAuthStateListener(mAuthListener!!)
        }
    }

    private fun loginWithPassword() {
        val email = emailEditText!!.text.toString().trim { it <= ' ' }
        val password = passwordEditText!!.text.toString().trim { it <= ' ' }

        if (email == "") {
            emailEditText!!.error = "Please enter your email"
            return
        }

        if (password == "") {
            passwordEditText!!.error = "Password cannot be blank"
            return
        }

        mAuthProgressDialog!!.show()
        mAuth!!.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
            mAuthProgressDialog!!.dismiss()

            if (!task.isSuccessful) {

                Toast.makeText(this, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun createAuthProgressDialog() {
        mAuthProgressDialog = ProgressDialog(this)
        mAuthProgressDialog!!.setTitle("Loading...")
        mAuthProgressDialog!!.setMessage("Authenticating with Firebase...")
        mAuthProgressDialog!!.setCancelable(false)
    }

    override fun onClick(view: View) {

        if (view === registerTextView) {
            val intent = Intent(this, CreateAccountActivity::class.java)
            startActivity(intent)
            finish()
        }

        if (view === passwordLoginButton) {
            loginWithPassword()
        }

    }

    companion object {
        val TAG = LoginActivity::class.java.simpleName
    }
}