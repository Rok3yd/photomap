package com.example.detroidsandwich.photomap.UI

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.text.format.DateFormat
import android.view.*
import com.example.detroidsandwich.photomap.*
import kotlinx.android.synthetic.main.fragment_timeline.*

import com.google.firebase.auth.FirebaseAuth
import io.reactivex.disposables.Disposable
import kotlin.collections.ArrayList


class TimelineFragment : Fragment() {

    private lateinit var mUser: String
    private lateinit var mFireBaseManager: FireBaseManager
    private var mAdapter: TimelineListAdapter? = null
    private var mPhotoMaps = ArrayList<PhotoMap>()
    private var mPhotoSearch = ArrayList<PhotoMap>()
    private lateinit var mObservablePhotoMap : Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mUser = FirebaseAuth.getInstance().currentUser!!.uid
        mFireBaseManager = FireBaseManager()
        downloadFirebase()
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_timeline, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.layoutManager = LinearLayoutManager(activity)

    }

    override fun onResume() {
        updateUI()
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
        mObservablePhotoMap.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.menu_timeline, menu)

        val menuItem = menu!!.findItem(R.id.menu_search)
        val searchView = MenuItemCompat.getActionView(menuItem) as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean = false

            override fun onQueryTextChange(newText: String): Boolean {
                setSearchPhotoMap(newText)
                return false
            }
        })
    }

    private fun updateUI() {
        var date = ""
        mPhotoSearch.clear()
        mPhotoMaps.reversed().forEach { photomap ->

            if(MainActivity.mCategory[photomap.category]) {

                val photodate = DateFormat.format("MMMM yyyy", photomap.date).toString()

                if (date != photodate){
                    date = photodate
                    val header = PhotoMap()
                    header.descriptor = date
                    mPhotoSearch.add(header)
                }
                mPhotoSearch.add(photomap)
            }
        }
        mAdapter = TimelineListAdapter(activity, mPhotoSearch)
        recyclerView.adapter = mAdapter
    }

    private fun downloadFirebase(){
        mObservablePhotoMap = mFireBaseManager.getChildEven(mUser)
                .subscribe{ photomap ->
                    mPhotoMaps.add(photomap)
                    updateUI()
                }
    }

    private fun setSearchPhotoMap(query: String){
        mPhotoSearch.clear()
        mPhotoMaps.reversed().forEach { photomap ->
            if(photomap.date != 0L){
            if(photomap.descriptor.contains(query)){
                mPhotoSearch.add(photomap)
            }}
        }
        activity.runOnUiThread {
            mAdapter = TimelineListAdapter(activity, mPhotoSearch)
            recyclerView.adapter = mAdapter
            recyclerView.setHasFixedSize(true)
        }
    }
}


