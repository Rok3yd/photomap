package com.example.detroidsandwich.photomap

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import java.util.ArrayList
import android.net.Uri
import com.example.detroidsandwich.photomap.UI.PhotoDetailActivity
import kotlinx.android.synthetic.main.listitem_timeline.*
import kotlinx.android.synthetic.main.listheader_timeline.*


class TimelineListAdapter(private val context: Activity, photoMaps: ArrayList<PhotoMap>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_HEADER = 0
    private val TYPE_ITEM = 1
    private var mPhotoMaps = ArrayList<PhotoMap>()
    private var mContext: Context = context
    private lateinit var mFireBaseManager: FireBaseManager

    init {
        mPhotoMaps = photoMaps
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.listitem_timeline, parent, false)

        if (viewType == TYPE_ITEM) {
            return TimelineViewHolder(view)
        } else if (viewType == TYPE_HEADER) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.listheader_timeline, parent, false)
            return TimelineHeaderHolder(view)
        }
        return TimelineViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is TimelineViewHolder) {

            holder.bindPhotoMap(mPhotoMaps[position])

            mFireBaseManager = FireBaseManager()
            mFireBaseManager.downloadImageFireBase(mPhotoMaps[position].imageUrl)
            mFireBaseManager.setDownloadImageListener(
                    object :FireBaseManager.DownloadImageListener{
                        override fun onDownloadedImage(file: String) {
                            holder.bindDrawable(file)
                        }
                    })

        } else if (holder is TimelineHeaderHolder) {

            holder.bindPhotoMapHeader(mPhotoMaps[position])
        }
    }

    override fun getItemViewType(position: Int): Int =
            if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM

    private fun isPositionHeader(position: Int): Boolean = mPhotoMaps[position].date == 0L

    override fun getItemCount(): Int = mPhotoMaps.size

    inner class TimelineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private var mListImageView: ImageView
        private var mListDescriptionTextView: TextView
        private var mListDateTextView: TextView
        private var mListCategoryTextView: TextView

        private val mContext: Context = itemView.context
        private lateinit var mPhotoMap: PhotoMap

        init {
            itemView.setOnClickListener(this)
            mListDescriptionTextView = itemView.findViewById(R.id.listDescription)
            mListDateTextView = itemView.findViewById(R.id.listDate)
            mListCategoryTextView = itemView.findViewById(R.id.listCategory)
            mListImageView = itemView.findViewById(R.id.listImage)
        }

        fun bindPhotoMap(photomap: PhotoMap) {
            mPhotoMap = photomap

            mListDescriptionTextView.text = photomap.descriptor
            mListDateTextView.text = DateFormat.format("yyyy.MM.dd", photomap.date)
            when (mPhotoMap.category) {
                0 -> mListCategoryTextView.setText(R.string.defaults)
                1 -> mListCategoryTextView.setText(R.string.friends)
                2 -> mListCategoryTextView.setText(R.string.nature)
            }
            mListImageView.setImageResource(0)

        }

        fun bindDrawable(drawable: String) {
            mListImageView.setImageURI(Uri.parse(drawable))
        }

        override fun onClick(v: View) {
            mContext.startActivity(PhotoDetailActivity.newIntent(mContext, mPhotoMap))
        }
    }

    inner class TimelineHeaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var mListHeaderTextView: TextView? = null

        fun bindPhotoMapHeader(photoMap: PhotoMap) {
            mListHeaderTextView = itemView.findViewById(R.id.list_header)
            mListHeaderTextView!!.text = photoMap.descriptor
        }
    }
}

