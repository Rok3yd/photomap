package com.example.detroidsandwich.photomap

import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.io.File
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.os.AsyncTask.execute



class FireBaseManager{

    private lateinit var mDownloadImageListener : DownloadImageListener
    private lateinit var mChildListener: ChildListener

    interface ChildListener {
        fun onChildAdded(photomap: PhotoMap)
    }

    fun setAddChildEvenListener(listener:ChildListener) {
        mChildListener = listener
    }

    fun getChildEven(user: String):Observable<PhotoMap>{
        return Observable.create<PhotoMap> {emitter->

            FirebaseDatabase.getInstance().reference
                    .child(user)
                    .orderByChild("date")
                    .addChildEventListener(object : ChildEventListener {
                        override fun onChildAdded(dataSnapshot: DataSnapshot,prevChildKey:String?) {

                            val photomap = dataSnapshot.getValue(PhotoMap::class.java)!!

                            emitter.onNext(photomap)
                        }
                        override fun onChildMoved(dataSnapshot: DataSnapshot, prevChildKey:String?) {}
                        override fun onChildRemoved(p0: DataSnapshot?) {}
                        override fun onChildChanged(dataSnapshot: DataSnapshot,prevChildKey:String?) {}
                        override fun onCancelled(databaseError: DatabaseError) {}
                    })

        }

    }

    fun loadingChildValue(user: String,id: String):FireBaseManager{

        FirebaseDatabase.getInstance().reference
                .child(user).child(id)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val photomap = dataSnapshot.getValue(PhotoMap::class.java)!!
                        mChildListener.onChildAdded(photomap)
                    }
                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        return this
    }


    interface DownloadImageListener {
        fun onDownloadedImage(file: String)
    }

    fun setDownloadImageListener(listener:DownloadImageListener) {
        mDownloadImageListener = listener
    }

    fun downloadImageFireBase(path: String):FireBaseManager {
        val localFile = File.createTempFile("images", "jpg")
        FirebaseStorage.getInstance().reference.child(path)
                .getFile(localFile)
                .addOnSuccessListener { taskSnapshot ->

                    val filePath = localFile.path
                    mDownloadImageListener.onDownloadedImage(filePath)
                }
        return this
    }

    companion object {
        fun uploadImageFireBase(file: Uri, user: String,progressBar:ProgressBar,latLng: LatLng) {

            progressBar.progress = 0
            progressBar.visibility = View.VISIBLE
            val path = "images/" + user + "/" + ImageUtils.getPhotoFilename()
            FirebaseStorage.getInstance()
                    .reference.child(path)
                    .putFile(file)
                    .addOnProgressListener { taskSnapshot ->
                        val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount
                        progressBar.progress = progress.toInt()
                    }
                    .addOnSuccessListener { taskSnapshot ->
                        progressBar.visibility = View.GONE

                        val photomap = PhotoMap(latLng,"",path)

                        FirebaseDatabase.getInstance()
                                .reference
                                .child(user)
                                .child(photomap.date.toString())
                                .setValue(photomap)
                    }
        }

        fun uploadFireBase(user: String,photomap: PhotoMap) {

            FirebaseDatabase.getInstance()
                    .reference
                    .child(user)
                    .child(photomap.date.toString())
                    .setValue(photomap)


        }
    }
}



