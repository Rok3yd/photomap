package com.example.detroidsandwich.photomap.UI

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

import com.google.firebase.auth.FirebaseAuth
import android.support.v4.view.ViewPager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import android.view.*
import com.example.detroidsandwich.photomap.R
import com.example.detroidsandwich.photomap.UI.CategoryActivity.Companion.EXTRA_PHOTOMAP_CATEGORY


class MainActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var mAuthListener: FirebaseAuth.AuthStateListener
    private lateinit var mAdapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        mAuth = FirebaseAuth.getInstance()

        mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user == null) {
               login()
            }else{
                setupViewPager(viewPager)
                tabs.setupWithViewPager(viewPager)
            }
        }
    }

    public override fun onStart() {
        super.onStart()
        mAuth.addAuthStateListener(mAuthListener)
    }

    public override fun onStop() {
        super.onStop()
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener)
        }
    }

    private fun login(){
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun setupViewPager(viewPager: ViewPager) {
        mAdapter = ViewPagerAdapter(supportFragmentManager)
        mAdapter.addFragment(MapFragment(), "MAP")
        mAdapter.addFragment(TimelineFragment(), "TIMELINE")
        viewPager.adapter = mAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.menu_category) {

            val intent = CategoryActivity.newIntent(application, mCategory)
            startActivityForResult(intent, REQUEST_CODE_CATEGORY)

        }
        if (id == R.id.menu_sign_out) {
            FirebaseAuth.getInstance().signOut()
            login()
        }
        return true
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList:ArrayList<Fragment> = ArrayList()
        private val mFragmentTitleList:ArrayList<String> = ArrayList()

        override fun getItem(position: Int): Fragment = mFragmentList[position]

        override fun getCount(): Int = mFragmentList.size

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }
        override fun getPageTitle(position: Int): CharSequence = mFragmentTitleList[position]
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_CATEGORY) {

                mCategory = data!!.getBooleanArrayExtra(EXTRA_PHOTOMAP_CATEGORY)
            }
            else{
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    companion object {
        var mCategory: BooleanArray = booleanArrayOf(true,true,true)
        val REQUEST_CODE_CATEGORY = 34
    }
}

