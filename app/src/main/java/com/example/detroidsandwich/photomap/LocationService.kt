package com.example.detroidsandwich.photomap

import android.location.Location
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import com.google.android.gms.location.LocationRequest
import com.patloew.rxlocation.RxLocation
import io.reactivex.disposables.CompositeDisposable

class LocationService(private val rxLocation: RxLocation) {

    private val disposable = CompositeDisposable()
    private val locationRequest: LocationRequest

    private var locationUpdate: LocationUpdate? = null


    interface LocationUpdate {
        fun onLocationUpdate(location: Location)
    }

    init {
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5000)
    }

    fun attachView(locationUpdate: LocationUpdate) {
        this.locationUpdate = locationUpdate
    }

    fun detachView() {
        this.locationUpdate = null
        disposable.clear()
    }

    fun startLocationRefresh() {
        disposable.add(
                rxLocation.settings().checkAndHandleResolution(locationRequest)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe{ boolean -> getAddressObservable(boolean) }
        )
    }

    private fun getAddressObservable(success: Boolean){
        if (success) {
            rxLocation.location().updates(locationRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {location ->
                        locationUpdate!!.onLocationUpdate(location) }
        } else {
             rxLocation.location().lastLocation()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe{location -> locationUpdate!!.onLocationUpdate(location) }

        }
    }

    fun checkLocation(locationPermissionGranted: Boolean) {
        if(locationPermissionGranted) {
            startLocationRefresh()
        }
    }

}