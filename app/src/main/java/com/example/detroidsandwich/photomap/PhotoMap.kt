package com.example.detroidsandwich.photomap

import com.google.android.gms.maps.model.LatLng
import java.util.*



class PhotoMap {

    var longitude: Double = 0.0
    var latitude: Double = 0.0
    var category: Int = 0
    var date: Long = 0
    lateinit var descriptor: String
    lateinit var imageUrl: String
    var thumbnail: String? = null

    constructor()

    constructor(longitude: Double, latitude: Double, descriptor: String, categoty: Int, imageUrl: String, thumbnail: String) {
        this.longitude = longitude
        this.latitude = latitude
        this.descriptor = descriptor
        this.category = categoty
        date = Date().time
        this.imageUrl = imageUrl
        this.thumbnail = thumbnail
    }

    constructor(latlng:LatLng,descriptor: String,imageUrl: String) {
        this.longitude = latlng.longitude
        this.latitude = latlng.latitude
        date = Date().time
        this.descriptor = descriptor
        this.imageUrl = imageUrl
    }

    constructor(latlng:LatLng,descriptor: String,imageUrl: String,thumbnail: String) {
        this.longitude = latlng.longitude
        this.latitude = latlng.latitude
        date = Date().time
        this.descriptor = descriptor
        this.imageUrl = imageUrl
        this.thumbnail = thumbnail
    }
}