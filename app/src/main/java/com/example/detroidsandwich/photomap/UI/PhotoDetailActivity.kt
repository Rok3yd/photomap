package com.example.detroidsandwich.photomap.UI

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import com.example.detroidsandwich.photomap.FireBaseManager
import com.example.detroidsandwich.photomap.PhotoMap
import com.example.detroidsandwich.photomap.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_photo_detail.*

class PhotoDetailActivity : AppCompatActivity() {

    private lateinit var mPhotoId: String
    private lateinit var mPhotoMap: PhotoMap
    private lateinit var mUser: String
    private lateinit var mSpinnerAdapter: ArrayAdapter<CharSequence>
    private lateinit var mFireBaseManager: FireBaseManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_detail)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mPhotoId = intent.getStringExtra(EXTRA_PHOTOMAP_ID)
        mUser = FirebaseAuth.getInstance().currentUser!!.uid

        mSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.category_photo, android.R.layout.simple_spinner_item)
        mSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        imageDetailView.setOnClickListener {
            val intent = FullscreenActivity.newIntent(application,mPhotoMap)
            startActivity(intent)
        }

        categorySpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                mPhotoMap.category = position
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    override fun onResume() {
        super.onResume()
        updateView(mUser, mPhotoId)
    }

    private fun updateView(user: String, id: String) {

        mFireBaseManager = FireBaseManager()
        mFireBaseManager.loadingChildValue(user,id)
                        .setAddChildEvenListener(object: FireBaseManager.ChildListener{
            override fun onChildAdded(photomap: PhotoMap) {
                mPhotoMap = photomap

                editDescriptorText.setText(mPhotoMap.descriptor)
                dataText.text = DateFormat.format("MMMM dd, yyyy - HH:mm a", mPhotoMap.date)
                categorySpinner.adapter = mSpinnerAdapter
                categorySpinner.setSelection(mPhotoMap.category)

                mFireBaseManager = FireBaseManager()
                mFireBaseManager.downloadImageFireBase(mPhotoMap.imageUrl)
                                .setDownloadImageListener(
                        object : FireBaseManager.DownloadImageListener {
                            override fun onDownloadedImage(file: String) {
                                imageDetailView.setImageURI(Uri.parse(file))

                            }
                        })
            }
        })
    }

    companion object {

        val EXTRA_PHOTOMAP_ID = "photomap_id"

        fun newIntent(packegeContext: Context, photoMap: PhotoMap): Intent {
            val intent = Intent(packegeContext, PhotoDetailActivity::class.java)
            intent.putExtra(EXTRA_PHOTOMAP_ID, photoMap.date.toString())
            return intent
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.menu_done) {

            mPhotoMap.descriptor = editDescriptorText.text.toString()

            FireBaseManager.uploadFireBase(FirebaseAuth.getInstance().currentUser!!.uid,mPhotoMap)

            finish()
        }
        if (id == android.R.id.home){
            finish()
            return true
        }
        return true
    }
}

