package com.example.detroidsandwich.photomap.UI

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_create_account.*
import android.view.View
import android.widget.Toast
import com.example.detroidsandwich.photomap.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest


class CreateAccountActivity : AppCompatActivity(), View.OnClickListener {

    private var mAuth: FirebaseAuth? = null
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null
    private var mAuthProgressDialog: ProgressDialog? = null
    private lateinit var mName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)

        mAuth = FirebaseAuth.getInstance()

        createAuthStateListener()
        createAuthProgressDialog()

        loginTextView!!.setOnClickListener(this)
        createUserButton!!.setOnClickListener(this)
    }

    public override fun onStart() {
        super.onStart()
        mAuth!!.addAuthStateListener(mAuthListener!!)
    }

    public override fun onStop() {
        super.onStop()
        if (mAuthListener != null) {
            mAuth!!.removeAuthStateListener(mAuthListener!!)
        }
    }

    override fun onClick(view: View) {

        if (view === loginTextView) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        if (view === createUserButton) {
            createNewUser()
        }

    }

    private fun createNewUser() {
        mName = nameEditText!!.text.toString().trim { it <= ' ' }
        val email = emailEditText!!.text.toString().trim { it <= ' ' }
        val password = passwordEditText!!.text.toString().trim { it <= ' ' }
        val confirmPassword = confirmPasswordEditText!!.text.toString().trim { it <= ' ' }

        val validEmail = isValidEmail(email)
        val validName = isValidName(mName!!)
        val validPassword = isValidPassword(password, confirmPassword)
        if (!validEmail || !validName || !validPassword) return

        mAuthProgressDialog!!.show()

        mAuth!!.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
            mAuthProgressDialog!!.dismiss()

            if (task.isSuccessful) {
                createFirebaseUserProfile(task.result.user)
            } else {
                Toast.makeText(this, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun createAuthProgressDialog() {
        mAuthProgressDialog = ProgressDialog(this)
        mAuthProgressDialog!!.setTitle("Loading...")
        mAuthProgressDialog!!.setMessage("Authenticating with Firebase...")
        mAuthProgressDialog!!.setCancelable(false)
    }

    private fun createFirebaseUserProfile(user: FirebaseUser) {
        val addProfileName = UserProfileChangeRequest.Builder()
                .setDisplayName(mName)
                .build()
        user.updateProfile(addProfileName)
                .addOnCompleteListener { task -> }
    }

    private fun createAuthStateListener() {
        mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            if (firebaseAuth.currentUser != null) {
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }
        }
    }


    private fun isValidEmail(email: String?): Boolean {
        val isGoodEmail = email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        if (!isGoodEmail) {
            emailEditText!!.error = "Please enter a valid email address"
            return false
        }
        return isGoodEmail
    }

    private fun isValidName(name: String): Boolean {
        if (name == "") {
            nameEditText!!.error = "Please enter your name"
            return false
        }
        return true
    }

    private fun isValidPassword(password: String, confirmPassword: String): Boolean {
        if (password.length < 6) {
            passwordEditText!!.error = "Please create a password containing at least 6 characters"
            return false
        } else if (password != confirmPassword) {
            passwordEditText!!.error = "Passwords do not match"
            return false
        }
        return true
    }

}
