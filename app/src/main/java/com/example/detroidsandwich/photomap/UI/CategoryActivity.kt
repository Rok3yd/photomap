package com.example.detroidsandwich.photomap.UI

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_category.*
import com.example.detroidsandwich.photomap.R


class CategoryActivity : AppCompatActivity() {

    lateinit var mCategory: BooleanArray

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mCategory = intent.getBooleanArrayExtra(EXTRA_PHOTOMAP_CATEGORY)

        changeCategory()

        dataDefault.setOnClickListener {
            mCategory[0] = !mCategory[0]
            changeCategory()
        }
        dataFriend.setOnClickListener {
            mCategory[1] = !mCategory[1]
            changeCategory()
        }
        dataNature.setOnClickListener {
            mCategory[2] = !mCategory[2]
            changeCategory()
        }

    }

    private fun changeCategory(){

        if(mCategory[0]){
            imageDefault.setImageResource(R.drawable.ic_radio_checked_24dp)
        }else{
            imageDefault.setImageResource(R.drawable.ic_radio_unchecked_24dp)
        }
        if(mCategory[1]){
            imageFriend.setImageResource(R.drawable.ic_radio_checked_24dp)
        }else{
            imageFriend.setImageResource(R.drawable.ic_radio_unchecked_24dp)
        }
        if(mCategory[2]){
            imageNature.setImageResource(R.drawable.ic_radio_checked_24dp)
        }else{
            imageNature.setImageResource(R.drawable.ic_radio_unchecked_24dp)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.menu_done) {

            val intent = Intent()
            intent.putExtra(EXTRA_PHOTOMAP_CATEGORY,mCategory)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        if (id == android.R.id.home){
            finish()
            return true
        }
        return true
    }

    companion object {

        val EXTRA_PHOTOMAP_CATEGORY = "photomap_category"

        fun newIntent(packegeContext: Context, category: BooleanArray): Intent {
            val intent = Intent(packegeContext, CategoryActivity::class.java)
            intent.putExtra(EXTRA_PHOTOMAP_CATEGORY,category)
            return intent
        }
    }
}
