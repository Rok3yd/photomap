package com.example.detroidsandwich.photomap

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.net.Uri
import android.os.Environment
import android.support.annotation.DrawableRes
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.util.Base64
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import java.io.*
import java.util.*

/**
 * Created by DetroidSandwich on 28.10.2017.
 */

object ImageUtils {

    fun getScaledBitmap(path: String, destWidth: Int): Bitmap {

        var options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, options)

        val srcWidth = options.outWidth.toFloat()
        val srcHeight = options.outHeight.toFloat()

        var inSampleSize = 1
        if (srcHeight > destWidth && srcWidth > destWidth) {
            if (srcWidth > srcHeight) {
                inSampleSize = Math.round(srcHeight / destWidth)
            } else {
                inSampleSize = Math.round(srcWidth / destWidth)
            }
        }
        options = BitmapFactory.Options()
        options.inSampleSize = inSampleSize
        return BitmapFactory.decodeFile(path, options)
    }

    fun getScaledBitmap(path: String): Bitmap {
        return getScaledBitmap(path, 200)
    }

    fun decodeFile(filePath: String, WIDTH: Int, HIGHT: Int): Bitmap? {
        try {

            val f = File(filePath)

            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeStream(FileInputStream(f), null, o)

            var scale = 1
            while (o.outWidth / scale / 2 >= WIDTH && o.outHeight / scale / 2 >= HIGHT)
                scale *= 2

            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            return BitmapFactory.decodeStream(FileInputStream(f), null, o2)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return null
    }

    @Throws(IOException::class)
    fun decodeFromFirebaseBase64(image: String): Bitmap {
        val decodedByteArray = android.util.Base64.decode(image, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedByteArray, 0, decodedByteArray.size)
    }

    fun getScaledFile(path: Uri,context: Context): String{

        val imageStream = context.contentResolver.openInputStream(path)
        val bitmap = BitmapFactory.decodeStream(imageStream)

        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos)
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)
    }


    fun vectorToBitmap(@DrawableRes id: Int, color: Int, context: Context): BitmapDescriptor {
        val vectorDrawable = ResourcesCompat.getDrawable(context.resources, id, null)
        val bitmap = Bitmap.createBitmap(
                vectorDrawable!!.intrinsicWidth,
                vectorDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
        DrawableCompat.setTint(vectorDrawable, context.resources.getColor(color))
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun getPhotoFilename(): String = "IMG_" + Date().time.toString() + ".jpg"

    fun getPhotoFile(context: Context): File? {
        val externalFilesDir: File = context
                .getExternalFilesDir(Environment.DIRECTORY_PICTURES) ?: return null
        return File(externalFilesDir, this.getPhotoFilename())
    }



}

