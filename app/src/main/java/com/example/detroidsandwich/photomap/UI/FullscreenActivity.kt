package com.example.detroidsandwich.photomap.UI

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.MenuItem
import android.support.v4.app.NavUtils
import android.text.format.DateFormat
import com.example.detroidsandwich.photomap.*

import kotlinx.android.synthetic.main.activity_fullscreen.*


class FullscreenActivity : AppCompatActivity() {
    private val mHideHandler = Handler()
    private lateinit var mContentView: TouchImageView
    private lateinit var mFireBaseManager: FireBaseManager

    private val mHidePart2Runnable = Runnable {

        mContentView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }
    private var mControlsView: View? = null
    private val mShowPart2Runnable = Runnable {

        val actionBar = supportActionBar
        actionBar?.show()
        mControlsView!!.visibility = View.VISIBLE
    }
    private var mVisible: Boolean = false
    private val mHideRunnable = Runnable { hide() }

    private val mDelayHideTouchListener = View.OnTouchListener { view, motionEvent ->
        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_fullscreen)
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)

        mVisible = true
        mControlsView = findViewById(R.id.fullscreen_content_controls)
        mContentView = findViewById<TouchImageView>(R.id.fullscreen_content)as TouchImageView

        mContentView.setOnClickListener { toggle() }

        val imageName = intent.getStringExtra(EXTRA_PHOTOMAP_IMAGE)
        val description = intent.getStringExtra(EXTRA_PHOTOMAP_DESCRIPTION)
        val date = intent.getLongExtra(EXTRA_PHOTOMAP_DATE,0)

        textDescription.text = description
        textDate.text = DateFormat.format("MMMM dd, yyyy - HH:mm a", date)

        mFireBaseManager = FireBaseManager()
        mFireBaseManager.downloadImageFireBase(imageName)
        mFireBaseManager.setDownloadImageListener(
                object :FireBaseManager.DownloadImageListener{
                    override fun onDownloadedImage(file: String) {
        mContentView.setImageURI(Uri.parse(file))

                    }
                })
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        delayedHide(100)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {

            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun toggle() {
        if (mVisible) {
            hide()
        } else {
            show()
        }
    }

    private fun hide() {

        val actionBar = supportActionBar
        actionBar?.hide()
        mControlsView!!.visibility = View.GONE
        mVisible = false

        mHideHandler.removeCallbacks(mShowPart2Runnable)
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    @SuppressLint("InlinedApi")
    private fun show() {

        mContentView!!.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        mVisible = true


        mHideHandler.removeCallbacks(mHidePart2Runnable)
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }


    private fun delayedHide(delayMillis: Int) {
        mHideHandler.removeCallbacks(mHideRunnable)
        mHideHandler.postDelayed(mHideRunnable, delayMillis.toLong())
    }

    companion object {

        private val AUTO_HIDE = true

        private val AUTO_HIDE_DELAY_MILLIS = 3000

        private val UI_ANIMATION_DELAY = 300

        val EXTRA_PHOTOMAP_IMAGE = "photomap_url_image"
        val EXTRA_PHOTOMAP_DATE = "photomap_url_date"
        val EXTRA_PHOTOMAP_DESCRIPTION = "photomap_url_description"

        fun newIntent(packegeContext: Context, photomap: PhotoMap): Intent {
            val intent = Intent(packegeContext, FullscreenActivity::class.java)
            intent.putExtra(EXTRA_PHOTOMAP_IMAGE, photomap.imageUrl)
            intent.putExtra(EXTRA_PHOTOMAP_DESCRIPTION, photomap.descriptor)
            intent.putExtra(EXTRA_PHOTOMAP_DATE, photomap.date)
            return intent
        }
    }
}
