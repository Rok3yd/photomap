package com.example.detroidsandwich.photomap.UI

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.*
import kotlinx.android.synthetic.main.fragment_map.*

import android.content.pm.PackageManager
import android.app.Activity
import android.content.*
import android.location.Location
import android.net.Uri
import android.provider.MediaStore
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.text.format.DateFormat
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.example.detroidsandwich.photomap.*
import com.example.detroidsandwich.photomap.R
import com.google.android.gms.maps.model.*
import com.google.firebase.auth.FirebaseAuth
import com.patloew.rxlocation.RxLocation
import io.reactivex.disposables.Disposable
import java.io.File
import java.io.IOException
import java.util.*


class MapFragment : Fragment(),
        OnMapReadyCallback,
        LocationService.LocationUpdate{

    private val CAMERA_REQUEST_CODE = 101
    private val GALLERY_REQUEST_CODE = 102
    private val PERMISSIONS_REQUEST_ACCESS_LOCATION = 103

    private var mLastKnownLocation: Location? = null
    private lateinit var mUser: String
    private lateinit var mGoogleMap: GoogleMap
    private lateinit var mPhotoMap: PhotoMap
    private lateinit var mLatLng: LatLng
    private lateinit var mOutputFileUri: Uri
    private lateinit var mFireBaseManager: FireBaseManager
    private var mMarkersPhotoMap = HashMap<String, PhotoMap>()
    private var mMarkerBitmap: Uri? = null
    private var mLocationPermissionGranted: Boolean = false
    private var mLocationSwitch: Boolean = false
    private lateinit var mLocationService : LocationService
    private lateinit var rxLocation : RxLocation
    private lateinit var mObservablePhotoMap : Disposable
    private val KEY_CAMERA = "camera_position"
    private var mCameraPosition: CameraPosition? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        mUser = FirebaseAuth.getInstance().currentUser!!.uid
        mFireBaseManager = FireBaseManager()
        rxLocation = RxLocation(activity)
        mLocationService = LocationService(rxLocation)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_map, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        updateValuesFromBundle(savedInstanceState)
        setButtonsState(false)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)

        try {
            MapsInitializer.initialize(activity.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        fabFollow.setOnClickListener{
            getLocationPermission()
            mLocationSwitch = true

        }

        fabDiscover.setOnClickListener{
            mLocationSwitch = false
            setButtonsState(mLocationSwitch)
        }

        fabPhoto.setOnClickListener{
            getLocationPermission()
            if(mLastKnownLocation != null) {
                mLatLng = getLastLatlng()
                imagePickerDialog()
            }else
                Snackbar.make(coordinatorLayout, "Searching your location...", Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        Log.e("0000","    ")
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(KEY_CAMERA)) {
                Log.e("111","    "+ mCameraPosition)
                mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA)
            }
        }
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle?) {
        Log.e("222","    "+ mCameraPosition + "  " +mGoogleMap.cameraPosition)
        savedInstanceState!!.putParcelable(KEY_CAMERA, mGoogleMap.cameraPosition)
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        mLocationService.attachView(this)
    }

    override fun onStop() {
        super.onStop()
        mLocationService.detachView()
        mObservablePhotoMap.dispose()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mGoogleMap = googleMap
        mGoogleMap.clear()
        if(mCameraPosition != null) {
            Log.e("333", "    " + mCameraPosition)
            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition))
        }
        addMarkersFirebase()
        mapListeners()
    }

    override fun onLocationUpdate(location: Location) {

            mGoogleMap.isMyLocationEnabled = true
            mGoogleMap.uiSettings.isMyLocationButtonEnabled = false
            mLastKnownLocation=location
            setButtonsState(mLocationSwitch)
            if (mLocationSwitch) {
                val zoom = mGoogleMap.cameraPosition.zoom
                if(zoom>14F)
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(getLastLatlng(),zoom))
                else
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(getLastLatlng(),14F))
            }

        Log.e("LOCATION UPDATE"," " + location.latitude + ", " + location.longitude)
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity.applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Log.e("PERMISSION"," LocationPermissionGranted FINE and COARSE")
            mLocationPermissionGranted = true
            mLocationService.checkLocation(mLocationPermissionGranted)
        } else {

            if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)){
                Snackbar.make(coordinatorLayout,
                        R.string.permission_rationale,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok) {
                            requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                                    PERMISSIONS_REQUEST_ACCESS_LOCATION)
                        }
                        .show()
            }
            else {
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        PERMISSIONS_REQUEST_ACCESS_LOCATION)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                    mLocationService.checkLocation(mLocationPermissionGranted)
                    Log.e("PERMISSION"," LocationPermissionGranted FINE " + mLocationPermissionGranted)
                }else{

                    Snackbar.make(coordinatorLayout,
                            R.string.permission_denied_explanation,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.settings){
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null)
                                intent.data = uri
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                startActivity(intent)
                            }
                            .show()
                }
            }
        }
    }

    private fun mapListeners() {

        mGoogleMap.setOnMapLongClickListener { latLng ->
            mLatLng = latLng
            imagePickerDialog()
        }

        mGoogleMap.setOnInfoWindowClickListener{
            val intent = PhotoDetailActivity.newIntent(activity, mPhotoMap)
            startActivity(intent)
        }

        mGoogleMap.setOnMarkerClickListener(GoogleMap.OnMarkerClickListener { marker ->
            for (entry in mMarkersPhotoMap.entries) {
                if (entry.key == marker.id) {
                    mPhotoMap = entry.value
                            mMarkerBitmap = null
                            mFireBaseManager.downloadImageFireBase(mPhotoMap.imageUrl)
                                            .setDownloadImageListener(object :FireBaseManager.DownloadImageListener{
                                        override fun onDownloadedImage(file: String) {
                                            mMarkerBitmap = Uri.parse(file)
                                            marker.showInfoWindow()
                                        }
                                    })
                            if (marker != null) {
                                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(getLastLatlng(mPhotoMap)))
                                marker.showInfoWindow()
                            }
                    return@OnMarkerClickListener true
                }
            }
            return@OnMarkerClickListener false
        })

        mGoogleMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            var view  = layoutInflater.inflate(R.layout.marker_window_info, null)

            override fun getInfoWindow(marker: Marker) = null

            override fun getInfoContents(marker: Marker): View? {

                val markerDate: TextView = view.findViewById(R.id.markerDate)
                val markerDescription: TextView = view.findViewById(R.id.markerDescription)
                val markerImage: ImageView  = view.findViewById(R.id.markerImage)

                markerDate.text = DateFormat.format("yyyy.MM.dd", mPhotoMap.date)
                markerDescription.text = mPhotoMap.descriptor
                if(mMarkerBitmap != null){
                    markerImage.setImageURI(mMarkerBitmap)
                }else{
                    markerImage.setImageResource(0)
                }
                return view
            }
        })
    }

    private fun addMarkersFirebase(){
        mMarkersPhotoMap.clear()
        mObservablePhotoMap = mFireBaseManager.getChildEven(mUser)
                .subscribe{ photomap ->
                    if(MainActivity.mCategory[photomap.category]) {
                        val marker: Marker = mGoogleMap.addMarker(MarkerOptions()
                                .position(LatLng(photomap.latitude, photomap.longitude)))
                        when (photomap.category) {
                            0 -> marker.setIcon(ImageUtils.vectorToBitmap(R.drawable.ic_place,
                                R.color.colorCategoryDefault, activity))
                            1 -> marker.setIcon(ImageUtils.vectorToBitmap(R.drawable.ic_place,
                                R.color.colorCategoryFriend, activity))
                            2 -> marker.setIcon(ImageUtils.vectorToBitmap(R.drawable.ic_place,
                                R.color.colorCategoryNature, activity))
                        }
                    mMarkersPhotoMap.put(marker.id, photomap)
                    }
                }
    }

    private fun imagePickerDialog() {

        val items = arrayOf<CharSequence>(getString(R.string.chooseFromGallery),getString(R.string.takePicture))
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(activity)
        alertDialog.setTitle("Photo")
        alertDialog.setItems(items) { _, i ->

            if (items[i] == getString(R.string.chooseFromGallery)) {
                val intent = Intent()
                Log.e("   ","                                   GALLERY")
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE)
            }
            if (items[i] == getString(R.string.takePicture)) {
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePictureIntent.resolveActivity(activity.packageManager) != null) {
                    var photoFile: File?= null
                    try {
                        photoFile = ImageUtils.getPhotoFile(activity)
                    } catch (ex: IOException) {
                        Log.e("CAMERA","                photoFile is hull")
                    }
                    if (photoFile != null) {
                        mOutputFileUri = FileProvider.getUriForFile(activity,
                                "com.example.android.fileprovider",
                                photoFile)
                        Log.e("CAMERA", "                       " + mOutputFileUri.path)

                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputFileUri)
                        startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE)
                    }
                }
            }
        }
        alertDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.e("mapFragment", "requestCode = " + requestCode + ", resultCode = " + resultCode)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_CODE){
                Log.e("   ","                                   GALLERY")
                FireBaseManager.uploadImageFireBase(data!!.data,mUser,progressBar,mLatLng)

            }
            if (requestCode == CAMERA_REQUEST_CODE){
                if (data == null) {
                    Log.e("   ","                                   CAMERA")
                    FireBaseManager.uploadImageFireBase(mOutputFileUri,mUser,progressBar,mLatLng)
            }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setButtonsState(requestingLocationUpdates: Boolean) {
        Log.i("GPS", "onRequestPermissionResult " + requestingLocationUpdates)
        if (requestingLocationUpdates) {
            fabFollow.visibility = View.GONE
            fabDiscover.visibility = View.VISIBLE
        } else {
            fabFollow.visibility = View.VISIBLE
            fabDiscover.visibility = View.GONE
        }
    }

    private fun getLastLatlng() = LatLng(mLastKnownLocation!!.latitude,mLastKnownLocation!!.longitude)
    private fun getLastLatlng(photoMap: PhotoMap) = LatLng(photoMap.latitude,photoMap.longitude)

}
