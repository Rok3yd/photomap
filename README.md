Photo Map

==========

![photomap](https://pp.userapi.com/c841426/v841426601/356b3/GfiRHs6x49A.jpg)


PhotoMap mobile is an application (on Android) which allows to make and organise photos. Each new photo should be placed on a map where a photo was taken. Users should have possibility to add a description to a photo, assign a photo to a category, ?nd needed photos by hashtags.
All user-generated content saved on a backend side. The app uses ?rebase as a backend solution (see [firebase](https://firebase.google.com/docs/android/setup)).



## Map screen
![map screen](https://pp.userapi.com/c841235/v841235478/397a3/vTLHIpii3ro.jpg)

This is main screen. It shows map and photos which were made by 
the user in PhotoMap application. Photos are displayed as 
colored markers on the map. Markers can have different colors 
depending on a category of the image. There are 3 available 
photo categories:

- Friends  (pink)

- Nature    (green)

- Default (blue)



User can ?lter categories by Category Button(button on toolbar).
The user can make a photo by pressing Camera Button.



## Photo Marker Popover

![photo marker](https://pp.userapi.com/c834101/v834101478/206f1/-eNVHEuCPY4.jpg)

All Markers on the map are clickable. If the user clicks a marker, a popover should appear above the selected marker as shown on the picture. The map should be centered on selected marker. 



## Timeline screen

![timeline screen](https://pp.userapi.com/c841235/v841235478/397ad/XZFMSo1VdN8.jpg)

Timeline screen shows the list of all the photos created by current user. The list is sorted by historical order: the most recent items are at the top. The list is divided into months as shown on the picture. 



##  Photo Detail

![photo detail](https://pp.userapi.com/c841235/v841235478/397b7/h47PZ7YRbPc.jpg)

When a photo is created, Photo Detailed screen should be opened. This screen is used to add a description to a newly created photo and assign the photo to a category. Also, this screen can be used for editing existing photo�s information.



##  Full Photo View

![full photo view](https://pp.userapi.com/c841235/v841235478/397c1/gqYM6zAHNM8.jpg)

Full Photo View shows fully-visible selected photo. The photo 
should be zoomable (by double-tap and pinch-gesture). 



## Categories

![categories](https://pp.userapi.com/c824409/v824409478/23740/VP2-0gs8usM.jpg)

Categories Screen allows to ?lter photo categories. 
A category can be selected/unselected by checkbox button 
(rounded colourful button). By default, all categories are 
enabled (checked)